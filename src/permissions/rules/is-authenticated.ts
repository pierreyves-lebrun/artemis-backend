import { rule } from 'graphql-shield'

export const isAuthenticated = rule()((parent, args, ctx) =>
  Boolean(ctx.request.userId)
)
