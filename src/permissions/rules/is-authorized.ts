import { rule } from 'graphql-shield'

export const isAuthorized = rule()(async (parent, args, ctx) => {
  const id = ctx.request.userId || ''
  const user = await ctx.prisma.user.findOne({
    where: {
      id
    }
  })

  return user.role === 'ADMIN'
})
