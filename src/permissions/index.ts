import { allow, shield } from 'graphql-shield'
import { isAuthorized } from 'permissions/rules/is-authorized'
import { isAuthenticated } from 'permissions/rules/is-authenticated'

export const permissions = shield({
  Query: {
    '*': isAuthenticated,
    users: isAuthorized,
    usersCount: isAuthorized,
    me: isAuthenticated
  },
  Mutation: {
    '*': isAuthenticated,
    login: allow,
    signup: allow
  }
})
