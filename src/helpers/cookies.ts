import jwt from 'jsonwebtoken'
import config from 'config'
import { ReponseWithToken, TokenPayload } from 'types'

export function generateToken(payload: TokenPayload) {
  const cookieSecret = config.get<string>('cookie.secret')
  return jwt.sign(payload, cookieSecret)
}

export function setCookie({ response, token }: ReponseWithToken) {
  response.cookie('token', `Bearer ${token}`, {
    httpOnly: true,
    maxAge: 1000 * 60 * 60 * 24
  })
}
