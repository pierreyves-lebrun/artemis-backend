import { hash } from 'bcrypt'
import { generateToken, setCookie } from 'helpers/cookies'
import { FieldResolver } from '@nexus/schema'

export const signup: FieldResolver<'Mutation', 'signup'> = async (
  parent,
  { username, email, password, role },
  ctx
) => {
  const existing = await ctx.prisma.user.findOne({
    where: {
      username
    }
  })

  if (existing) {
    throw Error(`User "${username}" already exists`)
  }

  const hashedPassword = await hash(password, 10)
  const user = await ctx.prisma.user.create({
    data: {
      username,
      email,
      password: hashedPassword,
      role
    }
  })

  const token = generateToken({ userId: user.id })
  setCookie({ response: ctx.response, token })

  return user
}
