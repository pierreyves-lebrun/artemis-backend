import { compare } from 'bcrypt'
import { generateToken, setCookie } from 'helpers/cookies'
import { FieldResolver } from '@nexus/schema'

export const login: FieldResolver<'Mutation', 'login'> = async (
  parent,
  { username, password },
  ctx
) => {
  const user = await ctx.prisma.user.findOne({
    where: {
      username
    }
  })

  if (!user) {
    throw new Error('Could not find a match for username and password')
  }

  const valid = await compare(password, user.password)

  if (!valid) {
    throw new Error('Could not find a match for username and password')
  }

  const token = generateToken({ userId: user.id })
  setCookie({ response: ctx.response, token })

  return user
}
