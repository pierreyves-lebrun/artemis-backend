import { FieldResolver } from '@nexus/schema'

export const usersCount: FieldResolver<'Query', 'usersCount'> = async (
  parent,
  args,
  ctx
) => ctx.prisma.user.count({ ...args })
