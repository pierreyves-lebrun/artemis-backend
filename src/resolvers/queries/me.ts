import { FieldResolver } from '@nexus/schema'

export const me: FieldResolver<'Query', 'me'> = async (parent, args, ctx) => {
  const id = ctx.request.userId || ''
  return ctx.prisma.user.findOne({
    where: {
      id
    }
  })
}
