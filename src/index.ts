import express from 'express'
import { ApolloServer } from 'apollo-server-express'
import cookieParser from 'cookie-parser'
// eslint-disable-next-line import/no-extraneous-dependencies
import { PrismaClient } from '@prisma/client'
import { permissions } from 'permissions'
import { applyMiddleware } from 'graphql-middleware'
import { schema as baseSchema } from 'schema'
import { authentication } from 'middlewares/authentication'

const app = express()
app.use(cookieParser())
app.use(authentication())

const schema = applyMiddleware(baseSchema, permissions)

const prisma = new PrismaClient()
const server = new ApolloServer({
  schema,
  context: ({ req, res }) => ({
    prisma,
    request: req,
    response: res
  })
})

const {
  env: { URIS_CONFIG, BACKEND_SERVICE_HOST, BACKEND_SERVICE_PORT }
} = process

const URIs = JSON.parse(URIS_CONFIG)

const cors = {
  origin: URIs,
  credentials: true
}

server.applyMiddleware({ app, cors, path: '/' })

app.listen({ port: BACKEND_SERVICE_PORT }, () =>
  console.log(`🚀 Server ready at http://${BACKEND_SERVICE_HOST}`)
)
