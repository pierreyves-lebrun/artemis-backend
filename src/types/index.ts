import { PrismaClient } from '@prisma/client'
import { Request, Response } from 'express'

export interface Context {
  request: Request
  response: Response
  prisma: PrismaClient
}

export interface TokenPayload {
  userId: string
}

export interface ReponseWithToken {
  response: Response
  token: string
}
