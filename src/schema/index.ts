import { Query } from 'schema/queries'
import { Mutation } from 'schema/mutations'
import { types } from 'schema/types'
import { PrismaClient } from '@prisma/client'
import { nexusPrismaPlugin } from 'nexus-prisma'
import { makeSchema } from '@nexus/schema'

const prisma = new PrismaClient()

export const schema = makeSchema({
  types: [Query, Mutation, types],
  context: () => ({ prisma }),
  plugins: [nexusPrismaPlugin()],
  outputs: {
    schema: `${__dirname}/../schema.graphql`,
    typegen: `${__dirname}/generated/nexus.ts`
  },
  typegenAutoConfig: {
    contextType: 'Context.Context',
    sources: [
      {
        source: '@prisma/client',
        alias: 'prisma'
      },
      {
        source: require.resolve('../types'),
        alias: 'Context'
      }
    ]
  }
})
