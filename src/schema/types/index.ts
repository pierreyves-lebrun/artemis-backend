import { User } from 'schema/types/user'
import { Role } from 'schema/types/role'

export const types = {
  User,
  Role
}
