import { enumType } from '@nexus/schema'

export const Role = enumType({
  name: 'Role',
  members: ['USER', 'ADMIN'],
  description: 'The user privileges'
})
