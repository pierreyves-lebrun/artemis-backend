import { mutationType, stringArg, arg } from '@nexus/schema'
import { login, signup } from 'resolvers/mutations'

export const Mutation = mutationType({
  definition(t) {
    t.crud.createOneUser()

    t.crud.updateOneUser()
    t.crud.deleteOneUser()
    t.crud.deleteManyUser()

    t.field('signup', {
      type: 'User',
      args: {
        username: stringArg({ nullable: false }),
        email: stringArg({ nullable: false }),
        password: stringArg({ nullable: false }),
        role: arg({ type: 'Role', nullable: false })
      },
      resolve: signup
    })

    t.field('login', {
      type: 'User',
      args: {
        username: stringArg({ nullable: false }),
        password: stringArg({ nullable: false })
      },
      resolve: login
    })
  }
})
