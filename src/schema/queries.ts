import { queryType, intArg, stringArg, arg } from '@nexus/schema'
import { me, usersCount } from 'resolvers/queries'

export const Query = queryType({
  definition(t) {
    t.crud.user()
    t.crud.users({ ordering: true, filtering: true })

    t.field('me', {
      type: 'User',
      resolve: me
    })

    t.field('usersCount', {
      type: 'Int',
      args: {
        where: arg({ type: 'UserWhereInput' })
      },
      resolve: usersCount
    })
  }
})
