import { verify } from 'jsonwebtoken'
import { Request, Response, NextFunction } from 'express'
import { TokenPayload } from 'types'
import config from 'config'

export function authentication() {
  return function(req: Request, res: Response, next: NextFunction): void {
    const { token } = req.cookies

    if (!token) {
      return next()
    }

    try {
      const cookieSecret = config.get<string>('cookie.secret')
      const bareToken: string = token.replace('Bearer ', '')
      const { userId } = verify(bareToken, cookieSecret) as TokenPayload

      req.userId = userId
    } catch (error) {
      res.clearCookie('token')
    }

    return next()
  }
}
