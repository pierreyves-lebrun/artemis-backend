FROM node:12

RUN mkdir -p /home/node/backend && chown -R node:node /home/node/backend

WORKDIR /home/node/backend

COPY --chown=node:node . .

USER node

RUN yarn

EXPOSE 4000

CMD [ "yarn", "run", "dev" ]
