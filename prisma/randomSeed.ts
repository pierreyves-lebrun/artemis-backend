import { PrismaClient, UserCreateInput } from '@prisma/client'

const prisma = new PrismaClient()

function generateString() {
  return Math.random()
    .toString(36)
    .substring(7)
}

function generateUser(): UserCreateInput {
  return {
    email: generateString(),
    username: generateString(),
    password: generateString(),
    role: 'USER'
  }
}

function generateUsers({ length = 10 }) {
  return Array.from({ length }, () => generateUser())
}

async function seed() {
  const randomUsers = generateUsers({ length: 500 })

  const finalArray = randomUsers.map(user =>
    prisma.user.create({
      data: {
        ...user
      }
    })
  )
  return Promise.all(finalArray)
}

seed()
  .catch(e => console.error(e))
  .finally(async () => {
    await prisma.disconnect()
  })
