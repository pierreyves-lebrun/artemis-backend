import { PrismaClient } from '@prisma/client'
import { hash } from 'bcrypt'

const prisma = new PrismaClient()

async function seed() {
  const password = await hash('admin', 10)

  await prisma.user.create({
    data: {
      email: 'admin@gmail.com',
      username: 'admin',
      password,
      role: 'ADMIN'
    }
  })
}

seed()
  .catch(e => console.error(e))
  .finally(async () => {
    await prisma.disconnect()
  })
